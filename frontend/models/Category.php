<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $description
 * @property string $long_description
 * @property string $url
 * @property string $icon
 * @property string $image
 * @property int $is_active
 * @property int $sort
 *
 * @property TourCategory[] $tourCategories
 * @property Tours[] $tours
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'description' => 'Description',
            'long_description' => 'Long Description',
        ];
    }

    /**
     * Gets query for [[TourCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTourCategories()
    {
        return $this->hasMany(TourCategory::className(), ['category_id' => 'id']);
    }

    /**
     * Gets query for [[Tours]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTours()
    {
        return $this->hasMany(Tours::className(), ['id' => 'tour_id'])->viaTable('tour_category', ['category_id' => 'id']);
    }

    public function getIcon()
    {
        if (empty($this->icon) ||
            !is_file(Yii::getAlias("@frontend") . '/web/images/category/icons/' . $this->icon)) {
            return '/images/category/icons/placeholder.png';
        }
        return '/images/category/icons/' . $this->icon;
    }

    public function getImage()
    {
        if (empty($this->image) ||
            !is_file(Yii::getAlias("@frontend") . '/web/images/category/' . $this->image)) {
            return '/images/category/placeholder.png';
        }
        return '/images/category/' . $this->image;
    }
}
