<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property int $id
 * @property int $tour_id
 * @property string $start
 * @property string $finish
 * @property int $booking_quantity
 *
 * @property Booking[] $bookings
 * @property Tour $tour
 */
class Event extends \yii\db\ActiveRecord
{
    public $duration;
    public $bookingQuantityActual;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tour_id', 'start', 'finish', 'booking_quantity'], 'required'],
            [['tour_id', 'booking_quantity'], 'integer'],
            [['start', 'finish'], 'safe'],
            [['tour_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tour::className(), 'targetAttribute' => ['tour_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_id' => 'Тур',
            'start' => 'Дата початку',
            'finish' => 'Дата завершення',
            'duration' => 'Тривалість (днів)',
            'booking_quantity' => 'Максимальна кількість бронювань',
            'bookingQuantityActual' => 'Поточна кількість бронювань',
            'bookingQuantityAvailable' => 'Доступна кількість бронювань',
        ];
    }

    /**
     * Gets query for [[Bookings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['event_id' => 'id']);
    }

    /**
     * Gets query for [[Tour]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tour::className(), ['id' => 'tour_id']);
    }


    public function getDuration()
    {
        $differenceFormat = '%d';
        $datetime1 = date_create($this->start);
        $datetime2 = date_create($this->finish);
        $interval = date_diff($datetime1, $datetime2);
        return $interval->format($differenceFormat);
    }

    public function getBookingQuantityActual()
    {
        $sum = Booking::find()->where(['event_id' => $this->id])->sum('quantity');
        if (!$sum) {
            $sum = 0;
        }
        return $sum;
    }

    public function getBookingQuantityAvailable()
    {
        return $this->booking_quantity - $this->getBookingQuantityActual();
    }

    public function isAvailableDates()
    {
        $date = date("Y-m-d");
        if ($this->start < $date) {
            return false;
        }
        return true;
    }

    public function isAvailableQuantity()
    {
        $bookingQuantity = $this->booking_quantity;
        $bookingQuantityActual = $this->getBookingQuantityActual();
        if ($bookingQuantityActual >= $bookingQuantity) {
            return false;
        }
        return true;
    }

    public function isAvailable()
    {
        if (!$this->isAvailableDates()) {
            return false;
        }
        if (!$this->isAvailableQuantity()) {
            return false;
        }
        return true;
    }

    public function getStatus()
    {

    }

}
