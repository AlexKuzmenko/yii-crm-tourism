<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property int $id
 * @property int $event_id
 * @property int $client_id
 * @property string $email
 * @property string $phone
 * @property int $quantity
 *
 * @property Client $client
 * @property Event $event
 */
class Booking extends \yii\db\ActiveRecord
{
    public $clientFullName;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'client_id', 'email', 'phone', 'quantity'], 'required'],
            [['event_id', 'client_id', 'quantity'], 'integer'],
            [['email', 'phone'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
            ['quantity', 'integer', 'max' => $this->event->getBookingQuantityAvailable()]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'client_id' => 'Client ID',
            'email' => 'Email',
            'phone' => 'Телефон',
            'quantity' => 'Кількість',
            'clientFullName' => 'Клієнт',
            'eventDates' => 'Дати',
            'tourTitle' => 'Назва туру'
        ];
    }

    /**
     * Gets query for [[Client]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * Gets query for [[Event]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    public function getClientFullName()
    {
        return $this->client->first_name . " " . $this->client->last_name;
    }
}
