<?php

namespace frontend\controllers;

use backend\models\EventSearch;
use frontend\models\Category;
use frontend\models\Event;
use frontend\models\TourCategory;
use Yii;
use frontend\models\Tour;
use frontend\models\TourSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TourController implements the CRUD actions for Tour model.
 */
class TourController extends Controller
{

    /**
     * Displays a single Tour model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $category_id)
    {
        $category = Category::findOne(['id' => $category_id]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'category' => $category
        ]);
    }


    /**
     * Finds the Tour model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tour the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tour::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
