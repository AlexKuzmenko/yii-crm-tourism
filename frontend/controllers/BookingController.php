<?php

namespace frontend\controllers;

use frontend\models\Client;
use frontend\models\Event;
use frontend\models\User;
use Yii;
use frontend\models\Booking;
use frontend\models\BookingSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BookingController implements the CRUD actions for Booking model.
 */
class BookingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Booking models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Booking model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Booking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($eventId)
    {
        //Отримуємо об'єкт події (поїздки)
        $event = Event::findOne(['id' => $eventId]);
        if (!$event->isAvailable()) {
            throw new ForbiddenHttpException("Event is not already available");
        }

        //Ініціюємо об'єкт нового бронювання
        $model = new Booking();
        //Задаємо поле ідентифікатора події для бронювання за відповідним GET-параметром
        $model->event_id = $eventId;

        //Отримуємо дані про поточного користувача та його профілю клієнта
        //(в разі їх наявності)
        if (Yii::$app->user) {
            $user = User::findOne(Yii::$app->user->getId());
            $client = Client::findOne(['user_id' => $user->id]);
            if ($client) {
                //Якщо клієнт існує, ініціюємо властивості email та phone для бронювання
                $model->email = $client->email;
                $model->phone = $client->phone;
            } else if ($user) {
                //Інакше якщо поточний користувач зареєстрований, ініціюємо властивість email
                $model->email = $user->email;
            }
        }


        //Якщо дані завантажені в модель з форми
        if ($model->load(Yii::$app->request->post())) {

            if (!$client) {
                //Якщо клієнта на знайдено, то намагаємось його знайти за введеним email
                $client = Client::findOne(['email' => $model->email]);
            }

            if (!$client) {
                //Якщо клієнта не знайдемо, створюємо нового клієнта і додаємо його в БД
                $client = new Client();
                $client->email = $model->email;
                $client->phone = $model->phone;
                if ($user) {
                    $client->user_id = $user->getId();
                }
                $client->save();
            }

            $model->client_id = $client->id;

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
            'event' => $event,
            'client' => $client,
        ]);
    }

    /**
     * Updates an existing Booking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Booking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Booking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Booking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Booking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
