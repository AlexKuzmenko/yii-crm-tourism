<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';


?>
<div class="site-index">

    <div class="jumbotron">
        <h1>CRM-Система "Туристичний оператор"</h1>

        <p class="lead">CRM-Система для ведення туристичного бізнесу пропонує функціонал для створення сайту туристичної агенції, з можливістю керування турами, категоріями турів, зображеннями та галереями зображень.</p>

        <p><a class="btn btn-lg btn-success" href="<?= Url::to('/category/index') ?>">Каталог турів</a></p>
    </div>

</div>
