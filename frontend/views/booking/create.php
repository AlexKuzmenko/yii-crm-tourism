<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Booking */

$this->title = 'Створити бронювання';
$this->params['breadcrumbs'][] = ['label' => 'Мої бронювання', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <table class="table table-bordered">
        <tr>
            <th>Тур</th>
            <th>Дата початку</th>
            <th>Дата завершення</th>
            <th>Дані клієнта</th>
        </tr>
        <tr>
            <td><?= Html::encode($event->tour->title) ?></td>
            <td><?= Html::encode($event->start) ?></td>
            <td><?= Html::encode($event->finish) ?></td>
            <td>
                Прізвище: <?= Html::encode($client->last_name) ?><br>
                Ім'я: <?= Html::encode($client->first_name) ?><br>
                Email: <?= Html::encode($client->email) ?><br>
                Телефон: <?= Html::encode($client->phone) ?>
            </td>
        </tr>
    </table>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
