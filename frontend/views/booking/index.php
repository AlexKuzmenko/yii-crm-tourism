<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мої бронювання';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                 'attribute' => 'event',
                'label' => 'Поїздка',
                'format' => 'raw',
                'value' => function ($model) {
                    return "<a href=" . Url::to(['event/index', 'EventSearch[tour_id]' => $model->event->tour->id]) . ">" . $model->event->tour->title . "</a>";
                }
            ],
            'event.start',
            'event.finish',
            'email:email',
            'phone',
            [
                'attribute' => 'clientFullName',
                'value' => function ($model) {
                    return $model->getClientFullName();
                }
            ],

            'quantity',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
