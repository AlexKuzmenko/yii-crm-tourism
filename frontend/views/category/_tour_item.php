<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="list-group-item d-flex">
    <div class="pr-2">
        <?= Html::img($model->tour->getImage(), ['style' => 'width: 150px; margin: 3px']) ?>
    </div>
    <div class="pr-2">
        <h4>
            <a href="<?= Url::to(['tour/view', 'id' => $model->tour->id, 'category_id' => $model->category_id]); ?>">
                <?= Html::encode($model->tour->title) ?>
            </a>
        </h4>
        <a href="<?= Url::to(['event/index', 'EventSearch[tour_id]' => $model->tour->id]) ?>" class="btn btn-primary">Графік поїздок</a>
    </div>


</div>

