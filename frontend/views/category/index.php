<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Наші тури';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'list-group',
        ],
        'itemOptions' => [
            'tag' => false,
        ],
        'layout' => "{summary}\n{items}\n{pager}",
        'summaryOptions' => ['style' => "width: 100%; margin: 5px"],
        'itemView' => '_category_item',
        'viewParams' => ['id' => $model->id]
    ]);
    ?>


</div>
