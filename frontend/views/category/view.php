<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Наші тури', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-3">
                <div class="list-group">
                    <?php foreach($categories as $categoryItem): ?>
                        <?php $active = ($categoryItem->id == $model->id) ? "active" : ""; ?>
                        <a href="<?= Url::to(['category/view', 'id' => $categoryItem->id]); ?>" class="list-group-item <?= $active ?>">
                            <?= $categoryItem->name ?>
                        </a>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-9">
                <?= $model->long_description ?>
                <div class="container-fluid">
                    <?= ListView::widget([
                        'dataProvider' => $tourProvider,
                        'options' => [
                            'class' => 'list-group',
                        ],
                        'itemOptions' => [
                            'tag' => false,
                        ],
                        'layout' => "{summary}\n{items}\n{pager}",
                        'summaryOptions' => ['style' => "width: 100%; margin: 5px"],
                        'itemView' => '_tour_item',
                        'viewParams' => ['id' => $model->id]
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>




</div>
