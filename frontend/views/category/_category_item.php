<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<a href="<?= Url::to(['category/view', 'id' => $model->id]); ?>" class="list-group-item list-group-item-action">
    <div class="d-flex">
        <div class="p-3">
            <?= Html::img($model->getIcon(), ['width' => '50px']) ?>
        </div>
        <div class="p-3">
            <h4>
                <?= Html::encode($model->name) ?>
            </h4>

        </div>
    </div>


</a>
