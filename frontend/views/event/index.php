<?php

use backend\models\Tour;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Графік поїздок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'tour_id',
                'format' => 'raw',
                'filter' => ArrayHelper::map(Tour::find()->asArray()->all(), 'id', 'title'),
                'value' => function ($model) {
                    return $model->tour->title;
                }
            ],

            'start',
            'finish',
            [
                'attribute' => 'duration',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getDuration();
                }
            ],
            'booking_quantity',
            [
                'attribute' => 'bookingQuantityActual',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getBookingQuantityActual();
                }
            ],
            [
                'attribute' => 'bookingQuantityAvailable',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getBookingQuantityAvailable();
                }
            ],
            [
                'attribute' => 'book',
                'format' => 'raw',
                'label' => 'Забронювати',
                'value' => function ($model) {
                    if ($model->isAvailable()) {
                        return "<a href=" . URL::to(['booking/create', 'eventId' => $model->id]) . " class='btn btn-primary'>Забронювати</a>";
                    } else {
                        return "<button class='btn btn-secondary' disabled>Не доступно</button>";
                    }
                }
            ],

        ],
    ]); ?>


</div>
