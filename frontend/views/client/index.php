<?php

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
    $buttons['view'] = function ($url, $model) {
        if (Yii::$app->user->can('Client')) {
            return Html::a(FA::icon('eye'), $url, [
                'title' => "View",
                "style" => "margin: 3px"
            ]);
        }
    };

    $buttons['update'] = function ($url, $model) {
        if (Yii::$app->user->can('clientUpdate', ['client' => $model])) {
            return Html::a(FA::icon('edit'), $url, [
                'title' => "Update",
                "style" => "margin: 3px"
            ]);
        }
    };


    $buttons['delete'] = function ($url, $model) {
        if (Yii::$app->user->can('Admin')) {
            return Html::a(FA::icon('trash'), $url, [
                'title' => "Delete",
                "style" => "margin: 3px",
                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                'data-method' => 'post',
            ]);
        }
    };
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'last_name',
            'email:email',
            'phone',
            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{view}{update}{delete}',
                'buttons' => $buttons,
            ],

        ],
    ]); ?>


</div>
