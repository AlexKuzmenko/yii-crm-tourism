<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\User */

$this->title = "Профіль клієнта " . $model->first_name . " " . $model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Клієнти', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>



    <div class="container">

        <div class="row justify-content-center">
            <div class="col-7 col-sm-3">
                <?= Html::img($model->getImage(), ['alt' => 'My logo', 'width' => '100%', 'class' => 'border']) ?>
                <?= Html::a('Редагувати зображення', ['update-image', 'id' => $model->id], ['class' => 'btn btn-primary mt-2']) ?>
            </div>


            <div class="col-7 col-sm-4">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [

                        'first_name',
                        'last_name',
                        'email:email',
                        'phone',

                    ],
                ]) ?>
                <?= Html::a('Редагувати профіль', ['update', 'id' => $model->id], ['class' => 'btn btn-primary mt-2']) ?>
            </div>
        </div>

    </div>



</div>
