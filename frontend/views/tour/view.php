<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Tour */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Наші тури', 'url' => ['category/index']];
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => ['category/view', 'id' => $category->id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tour-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-4">
            <?= Html::img($model->getImage(), ['style' => 'width:100%;']); ?>
        </div>
        <div class="col-12 col-md-6 col-lg-8">
            <p>
                <?= Html::a('Графік туру', ['event/index', 'EventSearch[tour_id]' => $model->id], ['class' => 'btn btn-primary']) ?>
            </p>

            <p>
                <?= $model->long_description ?>
            </p>
        </div>
    </div>



</div>
