<?php

namespace backend\controllers;

use Yii;
use backend\models\TourCategory;
use backend\models\TourCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TourCategoryController implements the CRUD actions for TourCategory model.
 */
class TourCategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TourCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TourCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TourCategory model.
     * @param integer $tour_id
     * @param integer $category_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($tour_id, $category_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($tour_id, $category_id),
        ]);
    }

    /**
     * Creates a new TourCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TourCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'tour_id' => $model->tour_id, 'category_id' => $model->category_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TourCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $tour_id
     * @param integer $category_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($tour_id, $category_id)
    {
        $model = $this->findModel($tour_id, $category_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'tour_id' => $model->tour_id, 'category_id' => $model->category_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TourCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $tour_id
     * @param integer $category_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($tour_id, $category_id)
    {
        $this->findModel($tour_id, $category_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TourCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $tour_id
     * @param integer $category_id
     * @return TourCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($tour_id, $category_id)
    {
        if (($model = TourCategory::findOne(['tour_id' => $tour_id, 'category_id' => $category_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
