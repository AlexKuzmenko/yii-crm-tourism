<?php

use backend\models\Tour;
use dosamigos\datepicker\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tour_id')->dropDownList(ArrayHelper::map(Tour::find()->asArray()->all(), 'id', 'title'))?>

    <?= $form->field($model, 'start')->widget(DateRangePicker::className(), [
        'attributeTo' => 'finish',
        'form' => $form, // best for correct client validation
        'language' => 'ua',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ],
    ])->label('Start and finish dates');?>



    <?= $form->field($model, 'booking_quantity')->textInput(['type' => 'number', 'min' => 1, 'value' => '20']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
