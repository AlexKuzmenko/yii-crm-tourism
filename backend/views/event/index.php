<?php

use backend\models\Category;
use backend\models\Tour;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Календар поїздок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <p>
        <?= Html::a('Створити поїздку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'tour_id',
                'format' => 'raw',
                'filter' => ArrayHelper::map(Tour::find()->asArray()->all(), 'id', 'title'),
                'value' => function ($model) {
                    return $model->tour->title;
                }
            ],
            'start',
            'finish',
            'booking_quantity',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
