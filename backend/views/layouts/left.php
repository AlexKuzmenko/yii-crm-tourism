<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Mykola Bazhenov</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                    [
                        'label' => 'Бронювання',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Список бронювання', 'icon' => 'file-code-o', 'url' => ['/booking/index'],],
                        ]
                    ],
                    [
                        'label' => 'Календар поїздок',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Графік поїздок', 'icon' => 'file-code-o', 'url' => ['/event/index'],],
                            ['label' => 'Нова поїздка', 'icon' => 'file-code-o', 'url' => ['/event/create'],],
                        ]
                    ],
                    [
                        'label' => 'Тури',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Список турів', 'icon' => 'file-code-o', 'url' => ['/tour/index'],],
                            ['label' => 'Віднести тури до категорій', 'icon' => 'file-code-o', 'url' => ['/tour-category/index'],],
                            ['label' => 'Додати новий', 'icon' => 'file-code-o', 'url' => ['/tour/create'],],
                        ]
                    ],
                    [
                        'label' => 'Категорії',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Список категорій', 'icon' => 'file-code-o', 'url' => ['/category/index'],],
                            ['label' => 'Нова категорія', 'icon' => 'file-code-o', 'url' => ['/category/create'],],
                        ]
                    ],
                    [
                        'label' => 'Клієнти',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Список клієнтів', 'icon' => 'file-code-o', 'url' => ['/client/index'],],
                            ['label' => 'Новий клієнт', 'icon' => 'file-code-o', 'url' => ['/client/create'],],
                        ]
                    ],


                ],
            ]
        ) ?>

    </section>

</aside>
