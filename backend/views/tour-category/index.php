<?php

use backend\models\Category;
use backend\models\Tour;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TourCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tour Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tour Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'tour_id',
                'value' => function ($model) {
                    return $model->tour->title;
                },
                'filter' => ArrayHelper::map(Tour::find()->asArray()->all(), 'id', 'title')
            ],
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return $model->category->name;
                },
                'filter' => ArrayHelper::map(Category::find()->asArray()->all(), 'id', 'name')
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
