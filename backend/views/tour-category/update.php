<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TourCategory */

$this->title = 'Update Tour Category: ' . $model->tour_id;
$this->params['breadcrumbs'][] = ['label' => 'Tour Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tour_id, 'url' => ['view', 'tour_id' => $model->tour_id, 'category_id' => $model->category_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tour-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
