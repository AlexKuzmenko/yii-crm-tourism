<?php
namespace backend\assets;
use yii\web\AssetBundle;

class AdminLtePluginAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte';
    public $css = [

    ];
    public $js = [

    ];
    public $depends = [
        'dmstr\web\AdminLteAsset',
    ];
}