<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tour".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $long_description
 * @property string $price
 * @property string $image
 * @property int $is_active
 * @property int $sort
 *
 * @property Event[] $events
 * @property TourCategory[] $tourCategories
 * @property Category[] $categories
 * @property TourGallery[] $tourGalleries
 * @property Gallery[] $galleries
 */
class Tour extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tour';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'long_description', 'price', 'image', 'is_active', 'sort'], 'required'],
            [['description', 'long_description'], 'string'],
            [['is_active', 'sort'], 'integer'],
            [['title', 'price', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'long_description' => 'Long Description',
            'price' => 'Price',
            'image' => 'Image',
            'is_active' => 'Is Active',
            'sort' => 'Sort',
        ];
    }

    /**
     * Gets query for [[Events]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['tour_id' => 'id']);
    }


    /**
     * Gets query for [[TourCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTourCategories()
    {
        return $this->hasMany(TourCategory::className(), ['tour_id' => 'id']);
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('tour_category', ['tour_id' => 'id']);
    }

    /**
     * Gets query for [[TourGalleries]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTourGalleries()
    {
        return $this->hasMany(TourGallery::className(), ['tour_id' => 'id']);
    }

    /**
     * Gets query for [[Galleries]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGalleries()
    {
        return $this->hasMany(Gallery::className(), ['id' => 'gallery_id'])->viaTable('tour_gallery', ['tour_id' => 'id']);
    }

    public function getCategoriesStr()
    {
        $arr = [];
        foreach($this->categories as $category) {
            $arr[] = $category->name;
        }
        return join(', ', $arr);
    }
}
