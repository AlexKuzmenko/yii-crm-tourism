<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $category_id
 * @property int $parent_id
 * @property int $is_active
 * @property int $is_main
 * @property string $url
 * @property string $name
 * @property string $description
 * @property string $long_description
 * @property int $sort_order
 * @property string $icon
 * @property string $image
 * @property string $background_image
 * @property string $background_image_header
 *
 * @property TourCategory[] $tourCategories
 * @property Tour[] $tours
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'is_active', 'is_main', 'url', 'name', 'description', 'long_description', 'sort_order', 'icon', 'image', 'background_image', 'background_image_header'], 'required'],
            [['parent_id', 'is_active', 'is_main', 'sort_order'], 'integer'],
            [['description', 'long_description'], 'string'],
            [['url', 'name', 'icon', 'image', 'background_image', 'background_image_header'], 'string', 'max' => 255],
            [['url'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'parent_id' => 'Parent ID',
            'is_active' => 'Is Active',
            'is_main' => 'Is Main',
            'url' => 'Url',
            'name' => 'Name',
            'description' => 'Description',
            'long_description' => 'Long Description',
            'sort_order' => 'Sort Order',
            'icon' => 'Icon',
            'image' => 'Image',
            'background_image' => 'Background Image',
            'background_image_header' => 'Background Image Header',
        ];
    }

    /**
     * Gets query for [[TourCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTourCategories()
    {
        return $this->hasMany(TourCategory::className(), ['category_id' => 'category_id']);
    }

    /**
     * Gets query for [[Tours]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTours()
    {
        return $this->hasMany(Tour::className(), ['tour_id' => 'tour_id'])->viaTable('tour_category', ['category_id' => 'category_id']);
    }
}
