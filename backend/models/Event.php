<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property int $id
 * @property int $tour_id
 * @property string $start
 * @property string $finish
 * @property int $booking_quantity
 *
 * @property Booking[] $bookings
 * @property Tour $tour
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tour_id', 'start', 'finish', 'booking_quantity'], 'required'],
            [['tour_id', 'booking_quantity'], 'integer'],
            [['start', 'finish'], 'safe'],
            [['start', 'finish'], 'date', 'format' => 'php:Y-m-d'],
            ['start', 'validateDates'],
            [['tour_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tour::className(), 'targetAttribute' => ['tour_id' => 'id']],
        ];
    }

    public function validateDates()
    {
        if (strtotime($this->finish) < strtotime($this->start)) {
            $this->addError('start', 'Please give correct Start and End dates');
            $this->addError('finish', 'Please give correct Start and End dates');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_id' => 'Tour ID',
            'start' => 'Start',
            'finish' => 'Finish',
            'booking_quantity' => 'Booking Quantity',
        ];
    }

    /**
     * Gets query for [[Bookings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['event_id' => 'id']);
    }

    /**
     * Gets query for [[Tour]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tour::className(), ['id' => 'tour_id']);
    }

}
