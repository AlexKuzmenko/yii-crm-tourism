<?php

namespace console\controllers;

use common\rbac\OwnProfileRule;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $client = $auth->createRole('Client');
        $admin = $auth->createRole('Admin');

        $auth->add($client);
        $auth->add($admin);

        $auth->addChild($admin, $client);

        $auth->assign($admin, 1);
        $auth->assign($client, 2);
        $auth->assign($client, 3);
    }

    public function actionUpdateProfilePermission()
    {
        $auth = Yii::$app->authManager;

        $clientUpdate = $auth->createPermission('clientUpdate');
        $auth->add($clientUpdate);
        $admin = $auth->getRole("Admin");
        $auth->addChild($admin, $clientUpdate);
    }


    public function actionUpdateOwnProfileRule()
    {
        $auth = Yii::$app->authManager;
        $clientUpdateOwnProfile = $auth->createPermission('clientUpdateOwnProfile');
        $rule = new OwnProfileRule();
        $auth->add($rule);
        $clientUpdateOwnProfile->ruleName = $rule->name;
        $auth->add($clientUpdateOwnProfile);

        $client = $auth->getRole("Client");
        $auth->addChild($client, $clientUpdateOwnProfile);

        $clientUpdate = $auth->getPermission('clientUpdate');
        $auth->addChild($clientUpdateOwnProfile, $clientUpdate);
    }

}
